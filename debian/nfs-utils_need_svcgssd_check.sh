#!/bin/sh
# vim: set sw=4 sts=4 et:

set -eu                # Always put this in Bourne shell scripts
IFS="`printf ' \n\t'`" # Always put this in Bourne shell scripts

#
# Parse the exports file, and determine whether we need svcgssd.
#
NEED_SVCGSSD=no

if [ -f /etc/exports ]; then
    exec 9<&0 </etc/exports

    while read EXPORT OPTS
    do
        case $EXPORT in
            ''|\#*)
                continue
                ;;
        esac
        MACHINE=""
        case $OPTS in
            *\(*\))
                MACHINE=${OPTS%(*}
                OPTS=${OPTS#*(}
                OPTS=${OPTS%)}
                ;;
        esac
        case $MACHINE in
            "gss/krb5"|"gss/krb5i"|"gss/krb5p")
                NEED_SVCGSSD=yes
                ;;
        esac
        OLDIFS="$IFS"
        IFS=","
        for OPT in $OPTS; do
            case "$OPT" in
                sec=krb5|sec=krb5i|sec=krb5p)
                    NEED_SVCGSSD=yes
                ;;
            esac
        done
        IFS="$OLDIFS"
    done

    exec 0<&9 9<&-
fi

# Turn on rpc-svcgssd.service if /etc/nfs.conf or /etc/nfs.conf.d/*.conf has
# enabled options for rpc.svcgssd.
if /usr/sbin/nfsconf --dump | grep -q '^\[svcgssd\]$'; then
    NEED_SVCGSSD=yes
fi

case "$NEED_SVCGSSD" in
    yes)
        exit 0 # Start rpc-svcgssd.service
        ;;
    no)
        exit 1 # Skip rpc-svcgssd.service
        ;;
    *)
        exit 255 # Error out
        ;;
esac
