#!/bin/sh
# vim: set sw=4 sts=4 et:

set -eu                # Always put this in Bourne shell scripts
IFS="`printf ' \n\t'`" # Always put this in Bourne shell scripts

# This code fragment is from nfs-common sysv init script.

#
# Parse the fstab file, and determine whether we need gssd. This code is
# partially adapted from the mountnfs.sh script in the sysvinit package.
#
NEED_GSSD=no

if [ -f /etc/fstab ]; then
    exec 9<&0 </etc/fstab

    while read DEV MTPT FSTYPE OPTS REST
    do
        case $DEV in
            ''|\#*)
                continue
                ;;
        esac
        OLDIFS="$IFS"
        IFS=","
        for OPT in $OPTS; do
            case "$OPT" in
                sec=krb5|sec=krb5i|sec=krb5p)
                    NEED_GSSD=yes
                ;;
            esac
        done
        IFS="$OLDIFS"
    done

    exec 0<&9 9<&-
fi

# Parse /etc/nfs.conf and /etc/nfs.conf.d/*.conf [gssd] for keytab used by
# rpc.gssd.
GSSDKEYTAB=`/usr/sbin/nfsconf --get gssd keytab-file || :`

# Parse /etc/krb5.conf for default keytab if not otherwise defined.
if test -z "$GSSDKEYTAB" -a -e /etc/krb5.conf; then
    GSSDKEYTAB=`sed -n -e '/^\[libdefaults\]/,/^\[/{/^\s*default_keytab_name\s*=/{s/^.*=\s*\(FILE:\)\?//;p}}' /etc/krb5.conf`
fi

# Use fallback keytab location if not otherwise defined.
if test -z "$GSSDKEYTAB"; then
    GSSDKEYTAB=/etc/krb5.keytab
fi

# Turn on rpc-gssd.service if /etc/nfs.conf or /etc/nfs.conf.d/*.conf has
# enabled options for rpc.gssd.
if /usr/sbin/nfsconf --dump | grep -q '^\[gssd\]$'; then
    NEED_GSSD=yes
fi

# Turn on rpc-gssd.service if keytab exists.
if test -e "$GSSDKEYTAB"; then
    NEED_GSSD=yes
fi

#echo "GSSDKEYTAB: ${GSSDKEYTAB}"
#echo "NEED_GSSD:  ${NEED_GSSD}"

case "$NEED_GSSD" in
    yes)
        exit 0 # Start rpc-gssd.service
        ;;
    no)
        exit 1 # Skip rpc-gssd.service
        ;;
    *)
        exit 255 # Error out
        ;;
esac
